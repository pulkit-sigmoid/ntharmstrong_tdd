from unittest import mock

import pytest
from Armstrong import *
from unittest.mock import MagicMock
from pytest import raises


class TestArmsStrongNumberFinder:

    @classmethod
    def setup_class(cls):
        print("\nSetup class")

    @classmethod
    def teardown_class(cls):
        print("\nTeardown class")

    @pytest.fixture()
    def armstrong(self):
        armstrong = Armstrong()
        return armstrong

    @pytest.fixture()
    def mock_open(self, monkeypatch):
        mock_file = MagicMock()
        mock_file.readline = MagicMock(return_value="TestCases.txt")
        mock_open = MagicMock(return_value=mock_file)
        monkeypatch.setattr("builtins.open", mock_open)
        return mock_open

    @mock.patch('Armstrong.Armstrong.readFromFile', return_value=['153', '1234'])
    def test_CallIMockreadFromFile1(self, mock_readFromFile1, armstrong):

        armstrong.isArmsstrong()
        mock_readFromFile1.assert_called_once

    def test_CanCreateInstanceOfArmstrongClass(self, armstrong):
        armstrong

    def test_CanICallIsArmstrongFunction(self, armstrong):
        armstrong.isArmstrong(6)

    def test_ReadFromFile(self, armstrong, mock_open, monkeypatch):
        mock_exists = MagicMock(return_value=True)
        monkeypatch.setattr("os.path.exists", mock_exists)
        result = armstrong.readFromFile("TestCases.txt")
        mock_open.assert_called_once_with("TestCases.txt", "r")

    def test_ReadFromFileThrowsExceptionFileNotExits(self, armstrong, mock_open, monkeypatch):
        mock_exists = MagicMock(return_value=False)
        monkeypatch.setattr("os.path.exists", mock_exists)
        with raises(Exception):
            result = armstrong.readFromFile("blah")

    def test_GetTrueWhen153PassedInIsArmstrongFucntion(self, armstrong):
        res = armstrong.isArmstrong(153)
        assert res == True

    def test_GetFalseWhen1253PassedInIsArmstrongFunction(self, armstrong):
        res = armstrong.isArmstrong(1253)
        assert res == False

    def test_Get9When9PassedInNthArmstrongFunction(self, armstrong):
        res = armstrong.NthArmstrong(9)
        assert res == 9

    def test_Get10When10PassedInNthArmstrongFunction(self, armstrong):
        res = armstrong.NthArmstrong(10)
        assert res == 153
